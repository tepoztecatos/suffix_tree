#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/hash.h"
#include "include/suffixArray.h"
#define MAX 1000

int main(int argc, char *argv[])
{
	//Se declaara apuntador raiz(ROOT)
	HashInt *hash=NULL;

	//Verificar entrada
	if(argv[1] == NULL)
	{
		printf("Wrong syntax!\n Example: ./main sequenceFile.txt\n");
		exit(EXIT_FAILURE);
	}

	//Abrimos archivo de secuencia
	FILE *Archivo;

	//Apuntador donde se almacenara el read del archivo
	char read[MAX];

	//Abrimos archivo dado desde los argumentos
	Archivo = fopen(argv[1], "r");
	if (Archivo == NULL)
	{
		exit(EXIT_FAILURE);
	}

	int  contador = 0;
	//Mientras haya lineas en el archivo
	while ( fgets(read, sizeof read , Archivo ) != NULL ) /* read a line */
	{
		//Verificar que no sea un comentario
		if(read[0] == '>')
		{
			contador++;
			continue;
		}
		else
		{
			//Conseguimos el tamano del string completo
                        int len=strlen(read);

                        //Removemos el salto de linea
                        read[len-1]=0;

                        //Guardamos nuevo valor de la secuencia(Sin salto de linea)
                        char *sequence=read;

                        //Ajustamos el tamano de nuevo
                        len=len-1;

			//Crear objeto de tipo Suffix
			Suffix *Suffix_Array;

			//Reservar memoria del tamaño a MAX_N multiplicado por el tamano de nuestra estructura de datos
			Suffix_Array=malloc(sizeof(Suffix) * (len+1) );

			//Creamos sufijos y los guardamos en el Suffix_Array
			int n=createSuffixArray(sequence, Suffix_Array, len);

			//printf("Tabla de sufijos correspondiente a %s\n", sequence);
			//printf("\n");

			//Imprimimos lista de sufijos
			//printSuffixArray(Suffix_Array, n);

			//TENEMOS LISTO NUESTRO ARREGLO DE OBJETOS SUFFIX
			//Ejemplo de iteracion
			int i;

			for(i=0; i<len+1; i++)
			{
				//Crear string temporal que convertira el lcp a char
				char tmp_concat[100];

				//Agregamos datos en la tabla de hash
				smartHashAddInt(&hash, Suffix_Array[i].sequence,Suffix_Array[i].lcp);
				//hashAddInt(&hash, Suffix_Array[i].sequence,Suffix_Array[i].lcp);
				//hashAdd(&hash, Suffix_Array[i].sequence,tmp_concat);
			}


			//Una vez agregada a la hash, ya podremos manipular la
			//informacion y liberar el arreglo de objetos Suffix_Array

			//printf("Tabla hash con los sufijos de %s\n", sequence);
			//printf("\n");


			//Liberamos la memoria, hay que evitar acumular datos basura
			free(Suffix_Array);
		}
	}
	imprimirListaInt(hash);
	printf("Secuencias procesadas: %d\n", contador);


	fclose(Archivo);

	return EXIT_SUCCESS;

}

