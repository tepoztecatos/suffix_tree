#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/hash.h"
#include "include/suffixArray.h"
#define MAX 1000

//Estructura de la tabla hash, ahora podemos definir un tipo de dato Hasg
/*
typedef struct tabla_hash
{
	char *key;
	char *value;
	struct tabla_hash *next_table;
} *Hash;


//Estructura de datos que usaremos para representar los nodos
typedef struct suffix_node
{
        //suf index
        int suffix;
        //String[suf]
        char *sequence;
        //Longest common suffix
        int lcp;

        //struct suffix_node *father_node;
}suffix_node; //Si se usa typedef, ya no sera necesario usar struct cada vez que se use esta estructura
*/

//Umbral del lcp para guardar en el hash
//Umbral>2

int main(int argc, char *argv[])
{
	printf("Prueba de hash y sus funciones\n");
	//Se declaara apuntador raiz(ROOT)
	Hash *hash=NULL;
	char key[MAX];
	char value[MAX*3];
	strcpy(key,"amarillo");
	strcpy(value,"banana");
	hashAdd(&hash, key, value);
	imprimirLista(hash);
	printf("\n");


	char secuencia[20];
	strcpy(secuencia, "GATAGACA$");
	printf("Vamos a construir la tablas Hash a partir de los sufijos de GATAGACA$ \n");
	printf("Las llaves son los sufijos de la secuencia del gen y los valores son el LCPI \n");

	//Crear sufijos con las funciones del programa de Array Suffix
	//Debe de estar dentro de una iteracion, supongamos que recorre linea por linea en un archivo de texto

	//Conseguimos string completo
	char sequence[]="ACTTCTTCGGC";
	int len=strlen(sequence);

	//Crear objeto de tipo Suffix
	Suffix *Suffix_Array;

	//Reservar memoria del tamaño a MAX_N multiplicado por el tamano de nuestra estructura de datos
	Suffix_Array=malloc(sizeof(Suffix) * (len+1) );

	//Creamos sufijos y los guardamos en el Suffix_Array
	int n=createSuffixArray(sequence, Suffix_Array, len);

	//Imprimimos lista de sufijos
	printSuffixArray(Suffix_Array, n);


	//TENEMOS LISTO NUESTRO ARREGLO DE OBJETOS SUFFIX
	//Ejemplo de iteracion
	int i;

	for(i=0; i<len; i++)
	{
		//Crear string temporal que convertira el lcp a char
		char tmp_concat[100];

		//Convertimos el lcp a char
		sprintf(tmp_concat, "%d", Suffix_Array[i].lcp);

		//Agregamos datos en la tabla de hash
		hashAdd(&hash, Suffix_Array[i].sequence,tmp_concat);
	}





	//Una vez agregada a la hash, ya podremos manipular la
	//informacion y liberar el arreglo de objetos Suffix_Array
	imprimirLista(hash);



	//Ejemplo de concatenacion, si se encuentra llave, agregar ';' y el segundo string
	int N;
	//Supongamos que agregaremos un 28 a la llave TTCGGC$
	char my_key[]="TTCGGC$";
	char value_Agregar[]="28";
	//Verificar primero si existe
	if(ifExists(hash, my_key, &N))
	{
		//Conseguimos el valor de la llave existente
		//y lo almacenamos para la concatenacion
		char *value_found=(char *)malloc(sizeof(char) * N);
		buscarLista(hash, my_key, value_found);

		//Concatenamos y guardamos la direccion en new_value
		char *new_value=hashConcat(value_found, value_Agregar); //(String1 + String2)

		//Sobrescrobimos el valor de la llave por la nueva concatenacion
		overWrite(&hash, my_key, new_value);

		//Liberamos la memoria, esto es muy importante para evitar errores
		free(value_found);
		free(new_value);
	}
	else
	{
		printf("No Existe!\n");
	}

	imprimirLista(hash);

	//SmartHashAdd funcion que intentara buscar la llave, en caso de
	//que exista esa llave entonces concatenara las cadenas, de lo
	//Contrario lo agrega a la tabla hash, en caso de error retorna 99;
	smartHashAdd(&hash, my_key, "7921");
	smartHashAdd(&hash, "HOLA", "Me agregaron!");
	imprimirLista(hash);

	//Liberamos la memoria, hay que evitar acumular datos basura
	free(Suffix_Array);

	return 0;


}
