//////////////////////////////////////////////////////////////////////////////////
//
//	  ____      _   _  _____  _____             __  __
//	 / __"| uU |"|u| ||" ___||" ___|   ___      \ \/"/
//	<\___ \/  \| |\| U| |_  U| |_  u  |_"_|     /\  /\
//	 u___) |   | |_| \|  _|/\|  _|/    | |     U /  \ u
//	 |____/>> <<\___/ |_|    |_|     U/| |\u    /_/\_\
//	  )(  (__(__) )(  )(\\,- )(\\,.-,_|___|_,-,-,>> \\_
//	 (__)        (__)(__)(_/(__)(_/\_)-' '-(_/ \_)  (__)
//	     _      ____     ____       _     __   __
//	 U  /"\  U |  _"\ U |  _"\ uU  /"\  u \ \ / /
//	  \/ _ \/ \| |_) |/\| |_) |/ \/ _ \/   \ V /
//	  / ___ \  |  _ <   |  _ <   / ___ \  U_|"|_u
//	 /_/   \_\ |_| \_\  |_| \_\ /_/   \_\   |_|
//	  \\    >> //   \\_ //   \\_ \\    >.-,//|(_
//	 (__)  (__(__)  (__(__)  (__(__)  (__\_) (__)
//
//
//			Oscar Lopez Acevedo
//		     Octavio Rodriguez Garcia
//		 "Por mi raza hablara el Espiritu"
//
//	/*Un arreglo de sufijos es un arreglo ordenado de
//	todos los sufijos de una cadena dada.
//	Esta estructura de datos es muy simple, pero sin
//	embargo es muy poderosa y es usada en algoritmos
//	de compresión de datos y dentro del campo de la
//	bioinformática , indización de textos completos,
//	entre otros.
//	*/
//
//
//	Paso 1: Partir las cadenas en subcadenas
//	Paso 2: Ordenar lexicograficamente el arreglo
//	Paso 3: Encontrar el LCP
//////////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Tamano maximo para el arreglo de sufijos
#define MAX_N 1000
#define MAX_SONS (1000*(1000/2 +1 ))

//Estructura de datos que usaremos para representar los nodos
typedef struct suffix_node
{
	//suf index
	int suffix;
	//String[suf]
	char *sequence;
	//Longest common suffix
	int lcp;

}Suffix; //Si se usa typedef, ya no sera necesario usar struct cada vez que se use esta estructura


//Prototipos
//Conseguira el substring de acuerdo al sufijo
void substr(char *STRING, int START, int LENGTH, char *RESULT_ARRAY);

//Limpiara la variable substring
void clean_substring(char *ARRAY, int LENGTH);

//Ordenara los arreglos
void sort_struct(Suffix *STRUCT_NODE, int N);

//Hace un swap entre estructuras, funciona con la funcion sort_struct
void swap_struct(Suffix *STRUCT_NODE, int START_POS, int SWAP_POS);

//Calcular el lcp de solo un sufijo
int compute_lcp(char *STRING1, char *STRING2);

//Imprimir el objeto
void printSuffixArray(Suffix *Suffix_Array, int n);

//Crear substrings
void makeSubstrings(char *substring, Suffix *Suffix_Array, char *root_String, int n);

//Conseguir el LCP de todos los sufijos
void getAllLcp(Suffix *Suffix_Array, int n);

//Crear arreglo de sufijos dandole un string y donde guardarlo
int createSuffixArray(char *mainString, Suffix *Suffix_Array, int len);

//Importamos funciones mencionadas
#include "suffixArray.c"

