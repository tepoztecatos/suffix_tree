//FUNCIONES

//-----------------------------------------------------------------------------------------------//

///////Verificar si existe llave en la tabla de hash
int ifExistsInt(HashInt *hash_root, char *key, int *N)
{
	//Creamos apuntador que ire recorriendo el camino de hash_root
	HashInt recorrredor_ptr=hash_root;

	//Mientras no sea el fin del camino
	while(recorrredor_ptr != NULL)
	{
		//Comparar si es la llave que buscamos
		if( strcmp(recorrredor_ptr->key,key) == 0 )
		{
			*N=recorrredor_ptr->value;
			return 1;
		}
		//Apuntar a la siguiente direccion analizar
		recorrredor_ptr=recorrredor_ptr->next_table;
	}
	//Retornar 0 en caso de que no se haya encontrado la llave deseada
	return 0;
}



//-------------------------------------------------------------------------------------------------//




void hashAddInt(HashInt **hash_root, char *key, int value)
{
	//Apuntadores que nos ayudaran a desplazarnos a travez de la ruta
	HashInt hash_atras=NULL;
	HashInt hash_actual=NULL;
	HashInt hash_siguiente=NULL;

	//Intentar de reservar memoria en el heap :D
	hash_siguiente=malloc(sizeof(HashInt));

	//Mientras se pueda reservar memoria
	if( hash_siguiente != NULL)
	{
		//Guardamos en el siguiente apuntador la llave y valor que se guardaran,
		//mas adelante se le agrega la direccion de este apuntador en
		//-> next_table para hacer el puente con la nueva isla
		//hash_siguiente->key=key;
		//hash_siguiente->value=value;
		hash_siguiente->key=malloc(sizeof(char)*(strlen(key)+1));
		hash_siguiente->value=value;
		strcpy( hash_siguiente->key, key);
		hash_siguiente->next_table=NULL;

		//Seleccionar inicio de lista
		hash_atras=NULL;
		hash_actual=*hash_root;

		//Recorrer lista mientras el hash_actual no este vacio
		while(hash_actual != NULL)
		{
			hash_atras=hash_actual;
			hash_actual=hash_actual->next_table;
		}
		//Verificar si HAY UN INICIO, si el hash_root esta vacio entonces le damos la memoria reservada del hash_siguiente
		if(hash_atras==NULL)
		{
			//Modificamos su direccion(El contenido del apuntador) para que apunte a la memoria reservada
			//Guarda la nueva direccion dentro de hash_root, reemplaza el valor
			*hash_root=hash_siguiente;
		}
		//De lo contrario se crea el puente y conectamos los pequeños cachos de memoria
		else
		{
			hash_atras->next_table=hash_siguiente;
		}
	}
}


//-------------------------------------------------------------------------------//

//Funcion que buscara llave en el hash
int buscarListaInt(HashInt *inicial, char *key, int value) //en Value se guardara lo encontrado
{
        HashInt hash=inicial;
        while(hash != NULL)
        {
		if(strcmp(hash->key,key) == 0)
		{
			value=hash->value;
			return 1;
		}
                hash=hash->next_table;
        }
	return 0;
}
//Si fue encontrado el elemento, retorna 1 de lo contrario 0


//--------------------------------------------------------------------------------//

//Funcion para sobrescribir valor de una llave
void overWriteInt(HashInt **hash_root, char *key, int value)
{
	//Apuntadores que nos ayudaran a desplazarnos a travez de la ruta
	HashInt hash_actual=NULL;

	//Mientras exista un camino
	if( hash_root != NULL)
	{
		//Seleccionar inicio de lista
		hash_actual=*hash_root;
		//Recorrer lista mientras el hash_actual no este vacio
		while(hash_actual != NULL)
		{
			if(strcmp(hash_actual->key,key)==0)
			{
				hash_actual->value=value;
				return;
			}
			hash_actual=hash_actual->next_table;
		}
	}
}


//------------------------------------------------------------------------------//

int smartHashAddInt(HashInt **hash, char *key, int value)
{
	//Valor del elemento a buscar
	int N;

	//Verificar primero si existe
	if(ifExistsInt(*hash, key, &N))
	{
		//overWrite(hash, key, new_value);
		overWriteInt(hash, key, N+value);

	}
	else
	{
		//Si no existe, agregamos registro nuevo
		hashAddInt(hash, key, value);
	}

	return 0;
}
