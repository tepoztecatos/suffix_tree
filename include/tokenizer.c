//funcion que dado un string y un delimitador devuelve una
//matriz de strings que dan los valores partidos por
//el delimitador
int strTokenizer(char *str, char c, char ***arr)
{
	int count = 1;
	int token_len = 1;
	int i = 0;
	char *p;
	char *t;

	p = str;
	while (*p != '\0')
	{
		if (*p == c)
		{
			count++;
		}
		p++;
	}

	*arr = (char**) malloc(sizeof(char*) * count);
	if (*arr == NULL)
	{
		exit(1);
	}
	p = str;
	while (*p != '\0')
	{
		if (*p == c)
		{
			(*arr)[i] = (char*) malloc( sizeof(char) * token_len );
			if ((*arr)[i] == NULL)
			{
				exit(1);
			}
			token_len = 0;
			i++;
		}
		p++;
		token_len++;
	}
	(*arr)[i] = (char*) malloc( sizeof(char) * token_len );
	if ((*arr)[i] == NULL)
	{
		exit(1);
	}
	i = 0;
	p = str;
	t = ((*arr)[i]);
	while (*p != '\0')
	{
		if (*p != c && *p != '\0')
		{
			*t = *p;
			t++;
		}
		else
		{
			*t = '\0';
			i++;
			t = ((*arr)[i]);
		}
		p++;
	}
	return count;
}

char* hashConcat(const char *s1, const char *s2)
{
	int len1 = strlen(s1);
	int len2 = strlen(s2);

	//Reservar memoria para nuevo string
	char *result = malloc(sizeof(len1+1+len2+1));//+1 for the zero-terminator

	char *delimiter=";";
	//Verificamos que si se haya podido reservar memoria
	if(result != NULL)
	{
		//Copiamos string al nuevo apuntador
		strcpy(result, s1);
		printf("Result\n");
		printf("%s\n", result);

		//Concatenamos el delimitador
		strcat(result, delimiter);
		printf("Result + Delimeter \n");
		printf("%s\n", result);

		//Concatenamos la otra parte del string
		strcat(result, s2);
		printf("Result + Delimeter +S \n");
		printf("%s\n", result);

	}
	return result;


}

/*
int main(void)
{
	char *string=hashConcat("Hola","PKO");
	printf("%s\n",string);
	//int count=split(string);
	char *otroConcat=hashConcat(string,"plplplplpl");
	printf("%s\n",otroConcat);
	free(string);
	string=hashConcat(otroConcat,"OOOSHI");
	free(otroConcat);

	char **tokenizer=NULL;

	int splts=split(string, ';', &tokenizer);
	int i;
	for(i=0; i<splts; i++)
	{
		printf("%s\n",tokenizer[i]);
	}
}
*/
