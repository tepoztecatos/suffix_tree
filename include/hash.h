#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tokenizer.c"

//##############################################//
//      Por mi raza hablara el espiritu         //
//      API TABLA de HASHES
//##############################################//

//Tamano maximo de la llave
#define MAX 1000

//Estructura de la tabla hash
typedef struct hash_table
{
	char *key;
	char *value;
	struct hash_table *next_table;
} *Hash;


typedef struct hash_tableInt
{
	char *key;
	int value;
	struct hash_tableInt *next_table;
} *HashInt;


//Funciones de la tabla hash

int ifExists(Hash *hash_root, char *key, int *); //Verificara si Existe la llave en la tabla

void hashAdd(Hash **hash_root, char *key, char *value);//Agregar una llave nueva al hash

void imprimirLista(Hash *inicial); //Imprimir la lista de forma bonita :D

void overWrite(Hash **hash_root, char *key, char *value); //Sobrescribir el valor actual en la tabla

void delKey(Hash *hash_root, char *key); //Borrar elemento de la lista

int buscarLista(Hash *inicial, char *key, char *value); //Buscar elemento en la lista

int smartHashAdd(Hash **hash, char *key, char *value_Agregar); //Agrega elemento a la lista y en caso de que exista entonces lo concatena
#include "hashInt.c"
#include "hash.c"
