//Funcion que hace substring desde un inicio asta el final del string y lo guarda en un arreglo
void substr(char *string, int init, int end, char *result)
{
	int i;
	int j=0;
	for(i=init; i<end; i++)
	{
		result[j]=string[i];
		j++;
	}
}



//Funcion que va a swappear las estructuras, se usara cuando se este ordenando
void swap_struct(Suffix *node, int pos, int swap_pos)
{
	//Crea estructura de datos temporal
	Suffix temp=node[pos];	//Se guarda en temporal
	node[pos] = node[swap_pos];	//Swap structures
	node[swap_pos]=temp;
}




//Funcion que ordenara por orden alfabetico las estructuras
void sort_struct(Suffix *node, int n)
{
	//Iteradores
	int i, j;
	//Strings que usaremos para comparar
	char *str;
	char *str2;

	//Se aplica un bubble sort para los strings de la estructura
	for (i = 0; i < n; i++)
	{
		for(j=0; j<n-1; j++)
		{
			//Conseguir los strings y guardarlos
			str=node[j].sequence;
			str2=node[j+1].sequence;

			//Comparar y conseguir la diferencia que hay entre String1 y String2
			int cmp = strcmp(str, str2);

			//Si esta mas grande el string, entonces mandarlo a la derecha
			if (cmp > 0)
			{
				//Hacer swap
				swap_struct(node, j, j+1);
			}
		}

	}
}

//Imprimir arreglo de sufijos
void printSuffixArray(Suffix *Suffix_Array, int n)
{

	//Print
	printf("|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|\n");
	printf("\ti\tSA[i]\tLCP[i]\tSuffix\n");

	int i;
	for (i = 0; i < n; i++)
	{
		//Vamos a cambiar el caracter '|' por '$' ya que '|' tiene mas peso en la tabla ASCII
		//Cambiar el ultimo caracter por '$'
		int seq_len=strlen(Suffix_Array[i].sequence)-1;
		Suffix_Array[i].sequence[seq_len]='$';
		printf("\t%d\t%d\t%d\t%s\n",i,Suffix_Array[i].suffix, Suffix_Array[i].lcp, Suffix_Array[i].sequence);
	}
	printf("|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|\n");
}



//Calcular el lcp Longest Common prefix
int compute_lcp(char *str, char *str2)
{
	int i;
	int lcp=0;
	int n1=strlen(str);
	int n2=strlen(str2);
	for(i=0; i < n1 && i < n2; i++)
	{
		//Si los caracteres son igules en ambos arreglos
		if ( str[i] == str2[i] )
		{
			//Se aumenta el tamano de lcp
			lcp++;
		}
		else
		{
			//Fin, ya no hay mas coincidencias
			return lcp;
		}
	}
}

void makeSubstrings(char *substring, Suffix *Suffix_Array, char *root_String, int n)
{
	int i;
	for(i=0; i<n; i++)
	{
		//Llenar la estructura, se creo el sufijo
		Suffix_Array[i].suffix=i;
		//Se obtiene informacion necesaria y se hace substring
		substr(root_String, i, n, substring);

		//Se almacena el substring en el arreglo de estructuras
		Suffix_Array[i].sequence=strdup(substring);

		//Limpiamos el arreglo substring
		memset(substring, 0, sizeof(char) * n);
	}
}

void getAllLcp(Suffix *Suffix_Array, int n)
{
	int i;
	//Calcular el Longest common prefix
	for(i=2; i < n; i++)
	{
		Suffix_Array[i].lcp=compute_lcp(Suffix_Array[i].sequence, Suffix_Array[i-1].sequence);
	}
}

//Funcion que dado un String devolvera un arreglo de objetos
int createSuffixArray(char *mainString, Suffix *Suffix_Array, int len)
{
	char *temp_string=mainString;
	char *root_String;

	//Reservamos memoria para agregar $ y '\0'
	root_String = (char *) malloc(sizeof(char) * len + 2);

	//Copiamos el string que teniamos
	strcpy(root_String, temp_string);

	//Guardamos nuevos valores
	root_String[len]='|'; // '|' Porque tiene mas peso en la tabla ASCII
	root_String[len+1]='\0';

	//Prefix  substring
	char *substring;
	substring = (char*) malloc( (len + 1) * sizeof(char) );

	//Guardamos el tamano del string
	int n = (int)strlen(root_String);

	//Create all the suffix substrings
	makeSubstrings(substring, Suffix_Array, root_String, n);

	//Sort the structure before printing
	sort_struct(Suffix_Array,n);

	//Get the lcp for all suffixes created
	getAllLcp(Suffix_Array, n);

	//Liberar memoria Usada
	free(root_String);
	free(substring);

	return n;

}
