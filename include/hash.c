//FUNCIONES

//-----------------------------------------------------------------------------------------------//

///////Verificar si existe llave en la tabla de hash
int ifExists(Hash *hash_root, char *key, int *N)
{
	//Creamos apuntador que ire recorriendo el camino de hash_root
	Hash recorrredor_ptr=hash_root;

	//Mientras no sea el fin del camino
	while(recorrredor_ptr != NULL)
	{
		//Comparar si es la llave que buscamos
		if( strcmp(recorrredor_ptr->key,key) == 0 )
		{
			*N=strlen((recorrredor_ptr->value));
			//printf("El tamano key %s con value %s es:%d\n", key,(recorrredor_ptr->value), strlen((recorrredor_ptr->value)));
			//printf("Se encontro N %zo   %s\n",strlen((recorrredor_ptr->value)),key);
			return 1;
		}
		//Apuntar a la siguiente direccion analizar
		recorrredor_ptr=recorrredor_ptr->next_table;
	}
	//Retornar 0 en caso de que no se haya encontrado la llave deseada
	return 0;
}



//-------------------------------------------------------------------------------------------------//




void hashAdd(Hash **hash_root, char *key, char *value)
{
	//Apuntadores que nos ayudaran a desplazarnos a travez de la ruta
	Hash hash_atras=NULL;
	Hash hash_actual=NULL;
	Hash hash_siguiente=NULL;

	//Intentar de reservar memoria en el heap :D
	hash_siguiente=malloc(sizeof(Hash));

	//Mientras se pueda reservar memoria
	if( hash_siguiente != NULL)
	{
		//Guardamos en el siguiente apuntador la llave y valor que se guardaran,
		//mas adelante se le agrega la direccion de este apuntador en
		//-> next_table para hacer el puente con la nueva isla
		//hash_siguiente->key=key;
		//hash_siguiente->value=value;
		hash_siguiente->key=malloc(sizeof(char)*strlen(key));
		hash_siguiente->value=malloc(sizeof(char)*strlen(value));
		strcpy( hash_siguiente->key, key);
		strcpy( hash_siguiente->value, value);
		hash_siguiente->next_table=NULL;

		//Seleccionar inicio de lista
		hash_atras=NULL;
		hash_actual=*hash_root;

		//Recorrer lista mientras el hash_actual no este vacio
		while(hash_actual != NULL)
		{
			hash_atras=hash_actual;
			hash_actual=hash_actual->next_table;
		}
		//Verificar si HAY UN INICIO, si el hash_root esta vacio entonces le damos la memoria reservada del hash_siguiente
		if(hash_atras==NULL)
		{
			//Modificamos su direccion(El contenido del apuntador) para que apunte a la memoria reservada
			//Guarda la nueva direccion dentro de hash_root, reemplaza el valor
			*hash_root=hash_siguiente;
		}
		//De lo contrario se crea el puente y conectamos los pequeños cachos de memoria
		else
		{
			hash_atras->next_table=hash_siguiente;
		}
	}
}


//-------------------------------------------------------------------------------//

//Funcion de imprimir
void imprimirLista(Hash *inicial)
{
        Hash hash=inicial;
	printf("KEY (Sequence) -> VALUE (LCP) \n");
        while(hash != NULL)
        {
                printf("%s->%s\n", (hash->key), (hash->value));

                hash=hash->next_table;
        }
    //printf("\n");

}


//Funcion de imprimir
void imprimirListaInt(HashInt *inicial)
{
        Hash hash=inicial;
	printf("KEY (Sequence) -> VALUE (LCP) \n");
        while(hash != NULL)
        {
                printf("%s->%d\n", (hash->key), (hash->value));

                hash=hash->next_table;
        }
    //printf("\n");

}


//--------------------------------------------------------------------------------//

//Funcion que buscara llave en el hash
int buscarLista(Hash *inicial, char *key, char *value) //en Value se guardara lo encontrado
{
        Hash hash=inicial;
        while(hash != NULL)
        {
		if(strcmp(hash->key,key) == 0)
		{
			strcpy(value, hash->value);
			return 1;
		}
                hash=hash->next_table;
        }
	return 0;
}
//Si fue encontrado el elemento, retorna 1 de lo contrario 0


//--------------------------------------------------------------------------------//

//Funcion para sobrescribir valor de una llave
void overWrite(Hash **hash_root, char *key, char *value)
{
	//Apuntadores que nos ayudaran a desplazarnos a travez de la ruta
	Hash hash_actual=NULL;

	//Mientras exista un camino
	if( hash_root != NULL)
	{
		//Seleccionar inicio de lista
		hash_actual=*hash_root;
		//Recorrer lista mientras el hash_actual no este vacio
		while(hash_actual != NULL)
		{
			if(strcmp(hash_actual->key,key)==0)
			{
				hash_actual->value=(char *)realloc(hash_actual->value, sizeof(strlen(value) + 1));
				strcpy(hash_actual->value,value);
				return;
			}
			hash_actual=hash_actual->next_table;
		}
	}
}


//------------------------------------------------------------------------------//

void delKey(Hash *hash_root, char *key)
{
	//Apuntadores que nos ayudaran a desplazarnos a travez de la ruta
	Hash hash_atras=NULL;
	Hash hash_actual=NULL;
	Hash tmpPtr;

        if(strcmp((*hash_root) -> key, key)==0)
        {
                tmpPtr=*hash_root;
                *hash_root = (*hash_root) -> next_table;
                free(tmpPtr);
                return;
        }
	else
	{
		hash_atras=*hash_root;
		hash_actual=(*hash_root)->next_table;
		while(hash_actual != NULL && strcmp(hash_actual->key, key) != 0 )
		{
			hash_atras=hash_actual;
			hash_actual=hash_actual->next_table;

		}
		//Despues de recorrer todo el camino, hash actual debe ser null,
		//a menos que la condicion del while no se haya cumplido
		//hash_actual->key != key

		if(hash_actual != NULL)
		{
			//Swap
			tmpPtr=hash_actual;
			hash_atras->next_table=hash_actual->next_table;
			free(hash_actual);
		}
	}
}

//-----------------------------------------------------------------------------------

int smartHashAdd(Hash **hash, char *key, char *value_Agregar)
{
	//Ejemplo de concatenacion, si se encuentra llave, agregar ';' y el segundo string
	int N;

	//Verificar primero si existe
	if(ifExists(*hash, key, &N))
	{
		//Conseguimos el valor de la llave existente
		//y lo almacenamos para la concatenacion
		char *value_found=(char *)malloc(sizeof(char) * N);
		if(value_found == NULL)
		{
			return 99;
		}
		buscarLista(*hash, key, value_found);

		//Concatenamos y guardamos la direccion en new_value
		char *new_value=malloc(sizeof(char) *(strlen(value_found) + strlen(new_value) + 1));

		char delimiter[]=";";

		//Copiamos string al nuevo apuntador
                strcpy(new_value, value_found);

		//Concatenamos el delimitador
		strcat(new_value, delimiter);

		//Concatenamos la otra parte del string
		strcat(new_value, new_value);

		//Sobrescrobimos el valor de la llave por la nueva concatenacion
		//overWrite(hash, key, new_value);
		overWrite(hash, key, value_found);

		//Liberamos la memoria, esto es muy importante para evitar errores
		free(value_found);
		free(new_value);
	}
	else
	{
		//Si no existe, agregamos registro nuevo
		hashAdd(hash, key, value_Agregar);
	}

	return 0;
}
